export const reduceUserPosts = (users, posts, userId) => {
  let usersWithPosts = []
  if (users) {
    usersWithPosts = users.map(user => {
      if (user.userId === userId) {
        return {
          ...user,
          posts: user.posts ? [...user.posts] : [...posts],
        }
      }
      return {
        ...user,
      }
    });
  }
  return usersWithPosts;
}

export const reduceUserPost = (users, post, userId) => {
  let usersWithPosts = []
  if (users) {
    usersWithPosts = users.map(user => {
      if (user.userId === userId) {
        return {
          ...user,
          posts: user.posts ? [...post, ...user.posts] : [...post],
        }
      }
      return {
        ...user,
      }
    });
  }
  return usersWithPosts;
}

export const reduceUserPostComments = (users, comments, userId, postId) => {
  let usersPostsWithComments = []
  if (users) {
    usersPostsWithComments = users && users.map(user => {
      if (user.userId === userId) {
        return {
          ...user,
          posts: user.posts && user.posts.map(userPost => {
            if (userPost.id === postId) {
              return {
                ...userPost,
                comments: userPost.comments ? [...userPost.comments] : [...comments],
              }
            }
            return {
              ...userPost,
            }
          }),
        }
      }
      return {
        ...user,
      }
    });
  }
  return usersPostsWithComments;
}

export const reduceUserPostComment = (users, comment, userId, postId) => {
  let usersPostsWithComments = []
  if (users) {
    usersPostsWithComments = users && users.map(user => {
      if (user.userId === userId) {
        return {
          ...user,
          posts: user.posts && user.posts.map(userPost => {
            if (userPost.id === postId) {
              return {
                ...userPost,
                comments: userPost.comments
                  ? [...comment, ...userPost.comments]
                  : [...comment],
              }
            }
            return {
              ...userPost,
            }
          }),
        }
      }
      return {
        ...user,
      }
    });
  }
  return usersPostsWithComments;
}