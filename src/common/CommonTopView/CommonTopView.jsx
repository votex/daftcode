import React from "react";
import "./styles.scss";

const CommonTopView = React.memo(() => {
  return (
    <>
      <div className="navigation">
        <span className="circle" />
        <span className="circle" />
        <span className="circle" />
      </div>
      <div className="placeholder">
        <span>https://daftcode.pl</span>
      </div>
    </>
  );
});

export default CommonTopView;
