import React from "react";
import "./styles.scss";

const Anchor = React.memo(({ text, href, target = "_self" }) => {
  return (
    <a className="anchor" href={href} target={target}>
      {text}
    </a>
  );
});

export default Anchor;
