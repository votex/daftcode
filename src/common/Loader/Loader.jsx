import React from "react";
import "./styles.scss";

const Loader = React.memo(() => {
  return <div className="loader" />;
});

export default Loader;
