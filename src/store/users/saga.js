import {
  FETCH_USERS,
  FETCH_USER_POSTS,
  FETCH_USER_POST_COMMENTS,
  ADD_POST,
  ADD_POST_COMMENT,
} from './types';
import { call, put, takeLatest, delay } from 'redux-saga/effects'
import { JSONPlaceHolderRepository } from '../../api/repositories/JSONPlaceholderRepository/JSONPlaceholderRepository';
import { userActions } from './actions';

function* fetchUsersSaga() {
  try {
    const usersData = yield call(JSONPlaceHolderRepository.fetchUsers);
    yield put(userActions.fetchUsersSuccess(usersData));
  } catch(e) {
    yield put(userActions.fetchUsersError());
    throw new Error("Some error occurred");
  }
}

function* fetchUserPostsSaga(action) {
  try {
    const { userId } = action.payload;
    yield delay(200);
    const userPosts = yield call(JSONPlaceHolderRepository.fetchUserPosts, userId);
    yield put(userActions.fetchUserPostsSuccess(userPosts, userId));
  } catch(e) {
    yield put(userActions.fetchUserPostsError());
    throw new Error("Some error occurred");
  }
}

function* fetchUserPostCommentsSaga(action) {
  try {
    const { userId, postId } = action.payload;
    yield delay(400);
    const userPostComments = yield call(JSONPlaceHolderRepository.fetchUserPostComments, postId);
    yield put(userActions.fetchUserPostCommentsSuccess(userPostComments, userId, postId));
  } catch(e) {
    yield put(userActions.fetchUserPostCommentsError());
    throw new Error("Some error occurred");
  }
}

function* addPostSaga(action) {
  try {
    const postId = yield call(JSONPlaceHolderRepository.addPost, {});
    const { body } = action.payload;
    yield put(userActions.addPostSuccess({ ...body, ...postId }));
  } catch(e) {
    yield put(userActions.addPostError());
    throw new Error("Some error occurred");
  }
}

function* addPostCommentSaga(action) {
  try {
    const { body, userId } = action.payload;
    yield put(userActions.addPostCommentSuccess(body, userId));
  } catch(e) {
    yield put(userActions.addPostCommentError());
    throw new Error("Some error occurred");
  }
}

function* userSaga() {
  yield takeLatest(FETCH_USERS, fetchUsersSaga);
  yield takeLatest(FETCH_USER_POSTS, fetchUserPostsSaga);
  yield takeLatest(FETCH_USER_POST_COMMENTS, fetchUserPostCommentsSaga);
  yield takeLatest(ADD_POST, addPostSaga);
  yield takeLatest(ADD_POST_COMMENT, addPostCommentSaga);
}

export default userSaga;