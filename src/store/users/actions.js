import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_ERROR,
  FETCH_USER_POSTS,
  FETCH_USER_POSTS_ERROR,
  FETCH_USER_POSTS_SUCCESS,
  FETCH_USER_POST_COMMENTS,
  FETCH_USER_POST_COMMENTS_ERROR,
  FETCH_USER_POST_COMMENTS_SUCCESS,
  ADD_POST,
  ADD_POST_SUCCESS,
  ADD_POST_ERROR,
  ADD_POST_COMMENT,
  ADD_POST_COMMENT_SUCCESS,
  ADD_POST_COMMENT_ERROR,
} from "./types";

export const userActions = {
  fetchUsers: () => ({ type: FETCH_USERS }),
  fetchUsersSuccess: (data) => ({
    type: FETCH_USERS_SUCCESS,
    payload: { data },
  }),
  fetchUsersError: () => ({ type: FETCH_USERS_ERROR }),
  fetchUserPosts: (userId) => ({
    type: FETCH_USER_POSTS,
    payload: { userId },
  }),
  fetchUserPostsSuccess: (data, userId) => ({
    type: FETCH_USER_POSTS_SUCCESS,
    payload: { data, userId },
  }),
  fetchUserPostsError: () => ({ type: FETCH_USER_POSTS_ERROR }),
  fetchUserPostComments: (userId, postId) => ({
    type: FETCH_USER_POST_COMMENTS,
    payload: { userId, postId },
  }),
  fetchUserPostCommentsSuccess: (data, userId, postId) => ({
    type: FETCH_USER_POST_COMMENTS_SUCCESS,
    payload: { data, userId, postId },
  }),
  fetchUserPostCommentsError: () => ({ type: FETCH_USER_POST_COMMENTS_ERROR }),
  addPost: (body) => ({
    type: ADD_POST,
    payload: { body },
  }),
  addPostSuccess: (postData) => ({
    type: ADD_POST_SUCCESS,
    payload: { postData },
  }),
  addPostError: () => ({ type: ADD_POST_ERROR }),
  addPostComment: (body, userId) => ({
    type: ADD_POST_COMMENT,
    payload: { body, userId },
  }),
  addPostCommentSuccess: (commentData, userId) => ({
    type: ADD_POST_COMMENT_SUCCESS,
    payload: { commentData, userId },
  }),
  addPostCommentError: () => ({ type: ADD_POST_COMMENT_ERROR }),
};
