import {
  FETCH_USERS_SUCCESS,
  FETCH_USER_POSTS_SUCCESS,
  FETCH_USER_POST_COMMENTS_SUCCESS,
  ADD_POST_SUCCESS,
  ADD_POST_COMMENT_SUCCESS
} from './types';
import {
  reduceUserPosts,
  reduceUserPost,
  reduceUserPostComments,
  reduceUserPostComment
} from '../../helpers/reduceHelpers';


const initialState = {
  users: null,
};

export function usersReducer(state = initialState, action) {
  switch(action.type) {
    case FETCH_USERS_SUCCESS: {
      return {
        ...state,
        users: action.payload.data,
      }
    }
    case FETCH_USER_POSTS_SUCCESS: {
      const { data, userId } = action.payload;
      const modifiedUsers = reduceUserPosts(state.users, data, userId);

      return {
        ...state,
        users: modifiedUsers,
      }
    }
    case FETCH_USER_POST_COMMENTS_SUCCESS: {
      const { data, userId, postId } = action.payload;
      const modifiedUsers = reduceUserPostComments(state.users, data, userId, postId);
      return {
        ...state,
        users: modifiedUsers,
      }
    }
    case ADD_POST_SUCCESS: {
      const { postData } = action.payload;
      const modifiedUsers = reduceUserPost(state.users, [postData], parseFloat(postData.userId));
      return {
        ...state,
        users: modifiedUsers,
      }
    }
    case ADD_POST_COMMENT_SUCCESS: {
      const { commentData, userId } = action.payload;
      const modifiedUsers = reduceUserPostComment(
        state.users,
        [commentData],
        parseFloat(userId),
        parseFloat(commentData.postId),
      );
      return {
        ...state,
        users: modifiedUsers,
      }
    }
    default:
      return state;
  }
}
