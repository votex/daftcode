export const usersSelector = (state) => state && state.usersReducer && state.usersReducer.users;
export const userPostsSelector = (state, userId) => {
  const users = usersSelector(state);
  if (!users) {
    return null;
  }
  const user = users.find(user => {
    return user.userId === parseFloat(userId);
  });
  return user && user.posts;
};
export const userNameSelector = (state, userId) => {
  const users = usersSelector(state);
  if (!users) {
    return null;
  }
  const user = users.find(user => {
    return user.userId === parseFloat(userId);
  });
  return user && user.username;
}
export const userPostSelector = (state, userId, postId) => {
  const userPosts = userPostsSelector(state, userId);
  const userPost = userPosts && userPosts.find(post => post.id === parseFloat(postId));
  return userPost;
}
export const userPostCommentsSelector = (state, userId, postId) => {
  const userPost = userPostSelector(state, userId, postId);
  return userPost && userPost.comments;
}
export const userPostCommentSelector = (state) => usersSelector(state) && usersSelector(state).posts;