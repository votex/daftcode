import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { usersReducer } from './users/reducers';
import userSaga from './users/saga';

const reducers = combineReducers({
  usersReducer,
})

let composeEnhancers = compose;

if (typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
}

const sagaMiddleware = createSagaMiddleware();

const enhancer = composeEnhancers(
  applyMiddleware(sagaMiddleware)
);

const store = createStore(
  reducers,
  enhancer,
);

sagaMiddleware.run(userSaga);


export default store;