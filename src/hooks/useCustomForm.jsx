import { useState, useEffect, useRef } from "react";

const useCustomForm = ({
  initialValues,
  onSubmit
}) => {
  const [values, setValues] = useState(initialValues || {});
  const [changeErrors, setChangeErrors] = useState({});

  const formRendered = useRef(true);

  useEffect(() => {
    if (!formRendered.current) {
      setValues(initialValues);
      setChangeErrors({});
    }
    formRendered.current = false;
  }, [initialValues]);

  const handleChange = (event) => {
    const { target } = event;
    const { name, value } = target;
    event.persist();

    setValues({ ...values, [name]: value });
  };

  const handleSubmit = (event) => {
    if (event) event.preventDefault();
    onSubmit({ values, changeErrors });
  };

  return {
    values,
    changeErrors,
    handleChange,
    handleSubmit
  };
};

export default useCustomForm;