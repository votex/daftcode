import { JSONPlaceHolderClient } from '../../clients/JSONPlaceholderClient/JSONPlaceholderClient';

export class JSONPlaceHolderRepository {
  static async fetchUsers() {
    const data = await JSONPlaceHolderClient.fetchUsers();
    
    return data.map(({id, name, username, email, phone, website, company}) => {
      return {
        name,
        username,
        email,
        phone,
        website,
        company,
        userId: id,
      }
    })
  }

  static async fetchUserPosts(userId) {
    const data = await JSONPlaceHolderClient.fetchUserPosts();
    const filteredPosts = data && data.filter(post => post.userId === userId);
    return filteredPosts;
  }

  static async fetchUserPostComments(postId) {
    const data = await JSONPlaceHolderClient.fetchUserPostComments(postId);
    return data;
  }

  static async addPost(body) {
    const data = await JSONPlaceHolderClient.addPost(body);
    return data;
  }
}