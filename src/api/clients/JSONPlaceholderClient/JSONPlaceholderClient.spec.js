import { JSONPlaceHolderClient } from './JSONPlaceholderClient';

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({}),
  })
);

const basePath = "https://jsonplaceholder.typicode.com";

beforeEach(() => {
  fetch.mockClear();
});

describe('Api tests', () => {
  it("should JSONPlaceholderClient get method throw error", async () => {
    fetch.mockImplementationOnce(() => Promise.reject("API is down"));
  
    await expect(JSONPlaceHolderClient.get()).rejects.toEqual(Error('Some error occurred'))
  });
  
  it("should JSONPlaceholderClient post method throw error", async () => {
    fetch.mockImplementationOnce(() => Promise.reject("API is down"));
  
    await expect(JSONPlaceHolderClient.post({})).rejects.toEqual(Error('Some error occurred'))
  });
  
  
  it("should JSONPlaceholderClient fetchUsers method to be call", async () => {
    jest.spyOn(JSONPlaceHolderClient, 'fetchUsers').mockImplementationOnce(() => Promise.resolve({ data: "cosik" }))
  
    expect(await JSONPlaceHolderClient.fetchUsers()).toEqual({ data: "cosik" });
    expect(await JSONPlaceHolderClient.fetchUsers).toBeCalledTimes(1);
  });
  
  it("should JSONPlaceholderClient fetchUsers method throw error", async () => {
    const error = new Error('Some error occurred');
    jest.spyOn(JSONPlaceHolderClient, 'fetchUsers').mockImplementationOnce(() => Promise.reject(error))
  
    await expect(JSONPlaceHolderClient.fetchUsers()).rejects.toEqual(error);
  });
  
  it("should JSONPlaceholderClient fetchUserPosts method to be call", async () => {
    jest.spyOn(JSONPlaceHolderClient, 'fetchUserPosts').mockImplementationOnce(() => Promise.resolve({ data: "cosik" }))
  
    expect(await JSONPlaceHolderClient.fetchUserPosts()).toEqual({ data: "cosik" });
    expect(await JSONPlaceHolderClient.fetchUserPosts).toBeCalledTimes(1);
  });
  
  it("should JSONPlaceholderClient fetchUserPosts method throw error", async () => {
    const error = new Error('Some error occurred');
    jest.spyOn(JSONPlaceHolderClient, 'fetchUserPosts').mockImplementationOnce(() => Promise.reject(error))
  
    await expect(JSONPlaceHolderClient.fetchUserPosts()).rejects.toEqual(error);
  });
  
  it("should JSONPlaceholderClient fetchUserPostComments method to be call", async () => {
    jest.spyOn(JSONPlaceHolderClient, 'fetchUserPostComments').mockImplementationOnce(() => Promise.resolve({ data: "cosik" }))
  
    expect(await JSONPlaceHolderClient.fetchUserPostComments()).toEqual({ data: "cosik" });
    expect(await JSONPlaceHolderClient.fetchUserPostComments).toBeCalledTimes(1);
  });
  
  it("should JSONPlaceholderClient fetchUserPostComments method throw error", async () => {
    const error = new Error('Some error occurred');
    jest.spyOn(JSONPlaceHolderClient, 'fetchUserPostComments').mockImplementationOnce(() => Promise.reject(error))
  
    await expect(JSONPlaceHolderClient.fetchUserPostComments()).rejects.toEqual(error);
  });
  
  it("should JSONPlaceholderClient addPost method to be call", async () => {
    jest.spyOn(JSONPlaceHolderClient, 'addPost').mockImplementationOnce(() => Promise.resolve({ data: "cosik" }))
  
    const mockBody = {
      data: "data",
    };
  
    expect(await JSONPlaceHolderClient.addPost(mockBody)).toEqual({ data: "cosik" });
    expect(await JSONPlaceHolderClient.addPost).toBeCalledWith(mockBody);
    expect(await JSONPlaceHolderClient.addPost).toBeCalledTimes(1);
  });
  
  it("should JSONPlaceholderClient addPost method throw error", async () => {
    const error = new Error('Some error occurred');
    jest.spyOn(JSONPlaceHolderClient, 'addPost').mockImplementationOnce(() => Promise.reject(error))
  
    await expect(JSONPlaceHolderClient.addPost()).rejects.toEqual(error);
  });
})
