export class JSONPlaceHolderClient {
  static config = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };

  static basePath = "https://jsonplaceholder.typicode.com";
  static usersEndpoint = "/users";
  static userPostsEndpoint = '/posts';
  static userPostCommentsEndpoint ='/posts/%s/comments';
  static addPostEndpoint = '/posts';

  static async get(path) {
    try {
      const response = await fetch(`${this.basePath + path}`, this.config);
      const data = await response.json();
      return data
    } catch {
      throw new Error('Some error occurred');
    }
  }

  static async post(path, body) {
    try {
      const response = await fetch(`${this.basePath + path}`, {
        method: 'POST',
        body: JSON.stringify(body),
        ...this.config.headers,
      });
      const data = await response.json();
      return data
    } catch {
      throw new Error('Some error occurred');
    }
  }

  static async fetchUsers() {
    return await this.get(this.usersEndpoint);
  }

  static async fetchUserPosts() {
    return await this.get(this.userPostsEndpoint);
  }

  static async fetchUserPostComments(postId) {
    const path = this.userPostCommentsEndpoint.replace('%s', postId);
    return await this.get(path);
  }

  static async addPost(body) {
    return await this.post(this.addPostEndpoint, body);
  }
}