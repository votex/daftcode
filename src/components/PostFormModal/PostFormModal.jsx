import React from "react";
import { useForm } from "react-hook-form";

const PostFormModal = ({ userId, callbackModal, callbackSendPost }) => {
  const { register, handleSubmit, errors } = useForm();

  React.useEffect(() => {
    document.body.style.overflow = "hidden";
  }, []);

  const handleCallback = () => {
    document.body.style.overflow = "auto";
    callbackModal(false);
  };

  const onSubmit = (data) => {
    const body = {
      userId: parseFloat(userId),
      body: data.content,
      title: data.title,
    };
    callbackSendPost(body);
    handleCallback();
  };

  return (
    <div className="modal">
      <div className="modal__container">
        <form onSubmit={handleSubmit(onSubmit)} className="modal__form">
          <h3>Add Post</h3>
          <label>Title</label>
          <input
            type="text"
            name="title"
            ref={register({
              required: "Title is required",
              minLength: {
                value: 5,
                message: "Minimum title length is 5 characters",
              },
            })}
          />
          {errors.title && (
            <p className="modal__error">{errors.title.message}</p>
          )}
          <br />
          <label>Content</label>
          <textarea
            type="text"
            name="content"
            ref={register({
              required: "Content is required",
              minLength: {
                value: 20,
                message: "Minimum content length is 20 characters",
              },
            })}
          />
          {errors.content && (
            <p className="modal__error">{errors.content.message}</p>
          )}
          <br />
          <button type="submit" className="submit">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default PostFormModal;
