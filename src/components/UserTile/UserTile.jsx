import React from "react";
import { useHistory } from "react-router-dom";
import Anchor from "../../common/Anchor/Anchor";
import "./styles.scss";

const UserTile = ({ name, email, website, phone, company, userId }) => {
  const history = useHistory();

  const handleOnClickTile = React.useCallback(() => {
    history.push(`/profile/${userId}/posts`);
  }, [history, userId]);

  return (
    <div onClick={() => handleOnClickTile()} className="tile">
      <h2 className="tile__title">{name}</h2>
      <div className="tile__anchor-container">
        <Anchor href={`mailto:${email}`} text={email} />
        <Anchor href={`tel:${phone}`} text={phone} />
        <Anchor href={`https://${website}`} text={website} target="_blank" />
      </div>
      <div className="tile__company-container">
        <p className="tile__text">{company.name}</p>
        <p className="tile__text">{company.catchPhrase}</p>
        <p className="tile__text">{company.bs}</p>
      </div>
      <button className="tile__button button">Details</button>
    </div>
  );
};

export default UserTile;
