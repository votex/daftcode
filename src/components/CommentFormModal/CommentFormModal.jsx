import React from "react";
import { v4 as uuidv4 } from "uuid";
import { useForm } from "react-hook-form";

const CommentFormModal = ({
  userId,
  callbackModal,
  callbackSendComment,
  postId,
}) => {
  const { register, handleSubmit, errors } = useForm();

  React.useEffect(() => {
    document.body.style.overflow = "hidden";
  }, []);

  const handleCallback = () => {
    document.body.style.overflow = "auto";
    callbackModal(false);
  };

  const onSubmit = (data) => {
    const body = {
      postId: parseFloat(postId),
      name: data.title,
      email: data.email,
      body: data.content,
      id: uuidv4(),
    };
    callbackSendComment(body, parseFloat(userId));
    handleCallback();
  };

  return (
    <div className="modal" noValidate>
      <div className="modal__container">
        <form onSubmit={handleSubmit(onSubmit)} className="modal__form">
          <h3>Add Comment</h3>
          <label>Title</label>
          <input
            type="text"
            name="title"
            autoComplete="off"
            ref={register({
              required: "Title is required",
              minLength: {
                value: 5,
                message: "Minimum title length is 5 characters",
              },
            })}
          />
          {errors.title && (
            <p className="modal__error">{errors.title.message}</p>
          )}
          <br />
          <label>E-mail</label>
          <input
            type="text"
            name="email"
            autoComplete="off"
            ref={register({ required: "Email is required" })}
          />
          {errors.email && (
            <p className="modal__error">{errors.email.message}</p>
          )}
          <br />
          <label>Content</label>
          <textarea
            name="content"
            ref={register({
              required: "Content is required",
              minLength: {
                value: 20,
                message: "Minimum content length is 20 characters",
              },
            })}
          />
          {errors.content && (
            <p className="modal__error">{errors.content.message}</p>
          )}
          <br />
          <button type="submit" className="submit">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default CommentFormModal;
