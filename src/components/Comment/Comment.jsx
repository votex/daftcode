import React from "react";
import "./styles.scss";

const Comment = ({ body, email, name }) => {
  return (
    <div className="comment">
      <div className="comment__container">
        <p className="comment__text comment__text--white">{name}</p>
        <p className="comment__text comment__text--green">{email}</p>
      </div>
      <p className="comment__text">{body}</p>
    </div>
  );
};

export default Comment;
