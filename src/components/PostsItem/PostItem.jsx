import React from "react";
import "./styles.scss";
import { useHistory } from "react-router-dom";
import { faTrash, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const PostItem = ({ title, userId, postId }) => {
  const history = useHistory();
  const handleOnClickTile = React.useCallback(() => {
    history.push(`/profile/${userId}/post/${postId}`);
  }, [history, userId, postId]);

  return (
    <div onClick={handleOnClickTile} className="post-item">
      <FontAwesomeIcon icon={faTrash} />
      <p>{title}</p>
      <FontAwesomeIcon icon={faChevronRight} />
    </div>
  );
};

export default PostItem;
