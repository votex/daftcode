import React from "react";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useHistory } from "react-router-dom";
import "./styles.scss";

const TopBar = ({ title }) => {
  const history = useHistory();

  return (
    <div className="top-bar">
      <FontAwesomeIcon onClick={() => history.goBack()} icon={faArrowLeft} />
      <h2>{title}</h2>
    </div>
  );
};

export default TopBar;
