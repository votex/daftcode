import React from "react";
import { connect } from "react-redux";
import { userActions } from "../../store/users/actions";
import { usersSelector } from "../../store/users/selectors";
import CommonTopView from "../../common/CommonTopView/CommonTopView";
import Loader from "../../common/Loader/Loader";
import "./styles.scss";
const UserTile = React.lazy(() => import("../../components/UserTile/UserTile"));

const DashboardContainer = ({ fetchUsers, users }) => {
  React.useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  return (
    <main className="dashboard container">
      <CommonTopView />
      <div className="dashboard__tile-container">
        <React.Suspense fallback={<Loader />}>
          {users &&
            users.map((user, i) => {
              return <UserTile key={i} {...user} />;
            })}
        </React.Suspense>
      </div>
    </main>
  );
};

const mapStateToProps = (state) => ({
  users: usersSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  fetchUsers: () => dispatch(userActions.fetchUsers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);
