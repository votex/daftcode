import React from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import {
  userNameSelector,
  userPostSelector,
} from "../../store/users/selectors";
import CommentsContainer from "../CommentsContainer/CommentsContainer";
import CommonTopView from "../../common/CommonTopView/CommonTopView";
import TopBar from "../../components/TopBar/TopBar";
import "./styles.scss";

const PostContainer = ({ userPost, userName }) => {
  const { userId, postId } = useParams();

  return (
    <main className="post container">
      <CommonTopView />
      <TopBar title={userName(userId)} />
      <div className="post__container">
        <h3 className="post__title">
          {userPost(userId, postId) && userPost(userId, postId).title}
        </h3>
        <p className="post__content">
          {userPost(userId, postId) && userPost(userId, postId).body}
        </p>
      </div>
      <CommentsContainer userId={userId} postId={postId} />
    </main>
  );
};

const mapStateToProps = (state) => ({
  userName: (userId) => userNameSelector(state, userId),
  userPost: (userId, postId) => userPostSelector(state, userId, postId),
});

export default connect(mapStateToProps, {})(PostContainer);
