import React from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { userActions } from "../../store/users/actions";
import {
  usersSelector,
  userPostsSelector,
  userNameSelector,
} from "../../store/users/selectors";
import CommonTopView from "../../common/CommonTopView/CommonTopView";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Loader from "../../common/Loader/Loader";
import PostFormModal from "../../components/PostFormModal/PostFormModal";
import TopBar from "../../components/TopBar/TopBar";
import "./styles.scss";

const PostItem = React.lazy(() =>
  import("../../components/PostsItem/PostItem")
);

const ProfileContainer = ({
  fetchUserPosts,
  fetchUsers,
  userPosts,
  userName,
  sendPost,
  users,
}) => {
  const { userId } = useParams();
  const [isModalOpen, setModal] = React.useState(false);
  const [isFetching, setFetching] = React.useState(!!users);

  React.useEffect(() => {
    fetchUserPosts(parseFloat(userId));
  }, [userId, fetchUserPosts, isFetching]);

  React.useEffect(() => {
    if (!isFetching) {
      setFetching(true);
      fetchUsers();
      fetchUserPosts(parseFloat(userId));
    }
  }, [userId, fetchUserPosts, fetchUsers, isFetching]);

  return (
    <main className="profile container">
      <CommonTopView />
      {isModalOpen && (
        <PostFormModal
          userId={userId}
          callbackModal={(bool) => setModal(bool)}
          callbackSendPost={sendPost}
        />
      )}
      <div className="post__navbar">
        <TopBar title={userName(userId)} />
        <div onClick={() => setModal(true)} className="add-modal">
          <FontAwesomeIcon icon={faPlus} />
        </div>
      </div>
      <div className="profile__posts-container">
        <React.Suspense fallback={<Loader />}>
          {userPosts(userId) &&
            userPosts(userId).map((post, i) => {
              return (
                <PostItem
                  key={i}
                  title={post.title}
                  userId={userId}
                  postId={post.id}
                />
              );
            })}
        </React.Suspense>
      </div>
    </main>
  );
};

const mapStateToProps = (state) => ({
  users: usersSelector(state),
  userName: (userId) => userNameSelector(state, userId),
  userPosts: (userId) => userPostsSelector(state, userId),
});

const mapDispatchToProps = (dispatch) => ({
  fetchUsers: () => dispatch(userActions.fetchUsers()),
  fetchUserPosts: (userId) => dispatch(userActions.fetchUserPosts(userId)),
  sendPost: (body) => dispatch(userActions.addPost(body)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
