import React from "react";
import { connect } from "react-redux";
import {
  userPostCommentsSelector,
  usersSelector,
} from "../../store/users/selectors";
import { userActions } from "../../store/users/actions";
import CommentFormModal from "../../components/CommentFormModal/CommentFormModal";
import Loader from "../../common/Loader/Loader";
import "./styles.scss";

const Comment = React.lazy(() => import("../../components/Comment/Comment"));

const CommentsContainer = ({
  userId,
  postId,
  userPostComments,
  fetchUserPostComments,
  sendComment,
  users,
  fetchUsers,
  fetchUserPosts,
}) => {
  const [showComments, setShowComments] = React.useState(false);
  const [isModalOpen, setModal] = React.useState(false);
  const [isFetching, setFetching] = React.useState(!!users);

  React.useEffect(() => {
    if (showComments) {
      fetchUserPostComments(parseFloat(userId), parseFloat(postId));
    }
  }, [showComments, userId, postId, fetchUserPostComments]);

  React.useEffect(() => {
    if (!isFetching) {
      setFetching(true);
      fetchUsers();
      fetchUserPosts(parseFloat(userId));
      if (showComments) {
        fetchUserPostComments(parseFloat(userId), parseFloat(postId));
      }
    }
  }, [
    showComments,
    userId,
    postId,
    fetchUserPosts,
    fetchUserPostComments,
    fetchUsers,
    isFetching,
  ]);

  return (
    <div className="comments">
      {showComments ? (
        <>
          {isModalOpen && (
            <CommentFormModal
              userId={userId}
              postId={postId}
              callbackModal={(bool) => setModal(bool)}
              callbackSendComment={sendComment}
            />
          )}
          <div className="comments__container">
            <div
              onClick={() => setShowComments(false)}
              className="comments--green"
            >
              Hide comments
            </div>
            <div onClick={() => setModal(true)} className="comments--green">
              Add comment
            </div>
          </div>
          <React.Suspense fallback={<Loader />}>
            {userPostComments(userId, postId) &&
              userPostComments(userId, postId).map((comment, i) => (
                <Comment
                  key={i}
                  body={comment.body}
                  email={comment.email}
                  name={comment.name}
                />
              ))}
          </React.Suspense>
        </>
      ) : (
        <div className="comments--green" onClick={() => setShowComments(true)}>
          Show comments
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  users: usersSelector(state),
  userPostComments: (userId, postId) =>
    userPostCommentsSelector(state, userId, postId),
});

const mapDispatchToProps = (dispatch) => ({
  fetchUsers: () => dispatch(userActions.fetchUsers()),
  fetchUserPosts: (userId) => dispatch(userActions.fetchUserPosts(userId)),
  fetchUserPostComments: (userId, postId) =>
    dispatch(userActions.fetchUserPostComments(userId, postId)),
  sendComment: (body, userId) =>
    dispatch(userActions.addPostComment(body, userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CommentsContainer);
