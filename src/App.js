import React from 'react';
import { Provider } from 'react-redux';
import './assets/styles/reset.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import store from './store/index';
import DashboardContainer from './containers/DashboardContainer/DashboardContainer';
import ProfileContainer from './containers/ProfileContainer/ProfileContainer';
import PostContainer from './containers/PostContainer/PostContainer';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/">
              <DashboardContainer />
            </Route>
            <Route path="/profile/:userId/posts">
              <ProfileContainer />
            </Route>
            <Route path="/profile/:userId/post/:postId">
              <PostContainer />
            </Route>
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
